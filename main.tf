provider "azurerm" {
  version = "~>2.10"
  features {}
}

provider "local" {
  version = "~>1.4"
}

resource "azurerm_resource_group" "clearcode" {
  name     = "ExampleClearCodeResourceGroup"
  location = "Japan East"
}


# Create a virtual network in the production-resources resource group
resource "azurerm_virtual_network" "clearcode" {
  name                = "clearcode-network"
  resource_group_name = azurerm_resource_group.clearcode.name
  location            = azurerm_resource_group.clearcode.location
  address_space       = ["10.5.0.0/16"]
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.clearcode.name
  virtual_network_name = azurerm_virtual_network.clearcode.name
  address_prefixes       = ["10.5.2.0/24"]
}

resource "azurerm_network_security_group" "testing" {
  name                = "BenchmarkSecurityGroup"
  location            = "Japan East"
  resource_group_name = azurerm_resource_group.clearcode.name

  security_rule {
    name                       = "RDP"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "WinRM"
    priority                   = 998
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5986"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "WinRM-out"
    priority                   = 100
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "5985"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Creating with Terraform"
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "testing" {
  network_interface_id      = azurerm_network_interface.testing.id
  network_security_group_id = azurerm_network_security_group.testing.id
}

resource "azurerm_public_ip" "testing" {
  name                    = "clearcode-collector-pip"
  location                = azurerm_resource_group.clearcode.location
  resource_group_name     = azurerm_resource_group.clearcode.name
  allocation_method       = "Dynamic"
  idle_timeout_in_minutes = 30

  tags = {
    environment = "clearcode-testing-pip"
  }
}

resource "azurerm_network_interface" "testing" {
  name                = "clearcode-testing-instance-nic"
  location            = azurerm_resource_group.clearcode.location
  resource_group_name = azurerm_resource_group.clearcode.name

  ip_configuration {
    name                          = "testing-instance-nic"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Static"
    private_ip_address            = "10.5.2.4"
    public_ip_address_id          = azurerm_public_ip.testing.id
  }
}

resource "azurerm_virtual_machine" "winservtesting" {
  name                             = "clearcode-testing-winserv-vm"
  location                         = azurerm_resource_group.clearcode.location
  resource_group_name              = azurerm_resource_group.clearcode.name
  network_interface_ids            = [azurerm_network_interface.testing.id]
  vm_size                          = "Standard_B2S"
  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }

  storage_os_disk {
    name              = "2019-datacenter-disk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
    os_type           = "Windows"
  }

  os_profile {
    computer_name  = "cc-winserv"
    admin_username = var.windows-username
    admin_password = var.windows-password
    custom_data    = file("./config/settings.ps1")
  }

  os_profile_windows_config {
    enable_automatic_upgrades = true
    provision_vm_agent        = true
    winrm {
      protocol = "http"
    }
    # Auto-Login's required to configure WinRM
    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "AutoLogon"
      content      = "<AutoLogon><Password><Value>${var.windows-password}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.windows-username}</Username></AutoLogon>"
    }
    # Unattend config is to enable basic auth in WinRM, required for the provisioner stage.
    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "FirstLogonCommands"
      content      = file("./config/FirstLogonCommands.xml")
    }
  }

  tags = {
    CreatedBy = var.windows-username
    Purpose   = "Describe Terraform instruction"
  }

  connection {
    host     = azurerm_public_ip.testing.ip_address
    type     = "winrm"
    port     = 5985
    https    = false
    timeout  = "2m"
    user     = var.windows-username
    password = var.windows-password
  }
}
